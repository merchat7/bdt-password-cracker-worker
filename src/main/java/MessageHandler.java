import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Timer;

public class MessageHandler implements Runnable {
    private Socket s;

    public MessageHandler(Socket s) {
        this.s = s;
    }

    public void run() {
        try {
            ObjectOutputStream dOut = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream dIn = new ObjectInputStream(s.getInputStream());

            handleRequest(dIn, dOut);
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new PingTask(timer, s), 0, 5000);

            handleCommand(dIn, dOut);
            // Shutdown?

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void handleRequest(ObjectInputStream dIn, ObjectOutputStream dOut) throws InterruptedException {
        try {
            int retry = 0;
            while (retry < 3) {
                if (!Message.sendRequest(dOut)) {
                    retry += 1;
                    Thread.sleep(1000);
                    continue;
                } else retry = 0;

                if (Message.receiveAckRequest(dIn)) break;
                else {
                    // Request packet was somehow corrupted or other errors
                    retry += 1;
                    Thread.sleep(1000);
                    // continue;
                }
            }
            if (retry == 3) throw new SocketTimeoutException();
        } catch (SocketTimeoutException e) {
            System.out.println("Time out limited exceeded");
            System.exit(1);
        }
    }

    private void handleCommand(ObjectInputStream dIn, ObjectOutputStream dOut) throws IOException {
        while (true) {
            s.setSoTimeout(0);
            String command = dIn.readUTF();
            if (command.equals("work")) {
                try {
                    Work work = (Work)dIn.readObject();
                    Message.ackWork(dOut);
                    Worker.works.put(work);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (command.equals("shutdown")) {
                System.out.println("Shutting down");
                System.exit(1);
            }
            else if (command.equals("cancel")) {
                if (Worker.cancelAble && !Worker.workExecutor.isShutdown()) {
                    System.out.println("Cancelling work");
                    Worker.isCancelled = true;
                    Worker.workExecutor.shutdownNow();
                }
            }
        }
    }
}
