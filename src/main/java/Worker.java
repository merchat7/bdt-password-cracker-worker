import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.*;

public class Worker {
    static BlockingQueue<Work> works = new LinkedBlockingDeque<Work>();
    static int noCores = Runtime.getRuntime().availableProcessors();
    static ExecutorService workExecutor = Executors.newFixedThreadPool(noCores);
    static CompletionService<String> completionService = new ExecutorCompletionService<String>(workExecutor);

    static boolean isCancelled;
    static boolean cancelAble;

    static String serverHost;
    static int serverPort;

    public static void main(String[] args) {
        serverHost = args[0];
        serverPort = (Integer.parseInt(args[1]));
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    Thread.sleep(200);
                    Socket s = new Socket(serverHost, serverPort);
                    Message.deregister(new ObjectOutputStream(s.getOutputStream()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        ExecutorService messageService = Executors.newSingleThreadExecutor(); // One thread dedicate to handling message
        try {
            Socket s = new Socket(serverHost, serverPort);
            s.setSoTimeout(15000); // 15 seconds as server will assume client is dead after this time
            messageService.submit(new MessageHandler(s));
        } catch (IOException e) {
            System.out.println("Error initializing the socket");
            e.printStackTrace();
            System.exit(1);
        }
        checkIfInRange();
    }

    private static void checkIfInRange() {
        while (true) {

            isCancelled = false;
            cancelAble = false;
            try {
                if (workExecutor.isShutdown()) {
                    workExecutor = Executors.newFixedThreadPool(noCores);
                    completionService = new ExecutorCompletionService<String>(workExecutor);
                }
                System.out.println("Waiting for work...");
                Work toDo = works.take(); // blocking here
                cancelAble = true;
                int total = toDo.getEnd() - toDo.getStart();
                int chunkSize = total / noCores;
                int start = toDo.getStart();
                int end = start + chunkSize;
                for (int i = 0; i < noCores; i++) {
                    if (i == (noCores - 1)) end = toDo.getEnd();
                    completionService.submit(new CrackerTask(i + 1, String.valueOf(toDo.getFirstThreeChars()), toDo.getHash(), start, end));
                    start += chunkSize;
                    end += chunkSize;
                }

                String returnMessage = "Not found yet";
                for (int j = 0; j < noCores; j++) {
                    try {
                        Future<String> result = completionService.take();
                        String resultContent = result.get();
                        if (resultContent != null) {
                            returnMessage = resultContent;
                            workExecutor.shutdownNow();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                if (!isCancelled) {
                    Socket s = new Socket(serverHost, serverPort);
                    s.setSoTimeout(15000);
                    handleResult(new ObjectInputStream(s.getInputStream()), new ObjectOutputStream(s.getOutputStream()), returnMessage);
                    System.out.println("Work done");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void handleResult(ObjectInputStream dIn, ObjectOutputStream dOut, String returnMessage) throws InterruptedException {
        try {
            int retry = 0;
            while (retry < 3) {
                if (!Message.sendResult(dOut, returnMessage)) {
                    retry += 1;
                    Thread.sleep(1000);
                    continue;
                } else retry = 0;

                if (Message.receiveAckResult(dIn)) break;
                else {
                    // Request packet was somehow corrupted or other errors
                    retry += 1;
                    Thread.sleep(1000);
                    // continue;
                }
            }
            if (retry == 3) throw new SocketTimeoutException();
        } catch (SocketTimeoutException e) {
            System.out.println("Time out limited exceeded");
            // TODO: Perhaps do not exit here?
            System.exit(1);
        }
    }
}
