import org.apache.commons.codec.digest.Crypt;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.Callable;

public class CrackerTask implements Callable<String> {
    private String validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    private int threadNo;

    private char[] charsStart;
    private String hash;
    private boolean isSpecial = false;
    private int start;
    private int end;

    private String foundPassword;

    public CrackerTask(int threadNo, String firstThreeChars, String hash, int start, int end) {
        this.threadNo = threadNo;
        this.charsStart = StringUtils.rightPad(firstThreeChars, 8).toCharArray();
        this.hash = hash;
        if (firstThreeChars.trim().length() == 0) this.isSpecial = true;
        this.start = start;
        this.end = end;
    }

    public String call() {
        try {
            System.out.println(String.format("Thread %d (start = %d, end = %d)", threadNo, start, end));
            if (isSpecial && start == 0 && end != 0) {
                start = -1; // Let the thread with start = 0 also check when charsStart[3] = " "
                for (int i = start; i < end; i++) {
                    if (i != -1) charsStart[3] = validChars.charAt(i);
                    System.out.println(String.format("Thread %d working on (%s....)", threadNo, String.valueOf(charsStart).substring(0, 4)));
                    if (crackSpecial() || crack()) return foundPassword;
                }
            } else {
                for (int i = start; i < end; i++) {
                    charsStart[3] = validChars.charAt(i);
                    System.out.println(String.format("Thread %d working on (%s....)", threadNo, String.valueOf(charsStart).substring(0, 4)));
                    if (crack()) return foundPassword;
                }
            }
        } catch (InterruptedException e) {
            System.out.println(String.format("Thread %d was interrupted", threadNo));
        }
        // Not tested
        if (String.valueOf(charsStart).equals("99999999")) return "Not found";
        return null;
    }

    // Iterate over every permutations of ONLY length 4
    private boolean crack() throws InterruptedException {
        char[] validCharsArray = validChars.toCharArray();
        for (char c1 : validCharsArray) {
            if (Thread.currentThread().isInterrupted()) throw new InterruptedException();
            charsStart[4] = c1;
            for (char c2 : validCharsArray ) {
                charsStart[5] = c2;
                for (char c3 : validCharsArray) {
                    charsStart[6] = c3;
                    for (char c4 : validCharsArray) {
                        charsStart[7] = c4;
                        String password = String.valueOf(charsStart).trim();
                        if (Crypt.crypt(password, "ic").equals(hash)) {
                            foundPassword = password;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    // Iterate over every permutations of length 0, 1, 2, 3
    private boolean crackSpecial()  {
        // charsStart[4] should be " "
        for (int i = -1; i < validChars.length(); i++) {
            if (i != -1)  charsStart[5] = validChars.charAt(i);
            for (int j = -1; j < validChars.length(); j++) {
                if (j != -1) charsStart[6] = validChars.charAt(j);
                for (int k = -1; k < validChars.length(); k++) {
                    if (k != -1)  charsStart[7] = validChars.charAt(k);
                    String password = String.valueOf(charsStart).trim();
                    if (Crypt.crypt(password, "ic").equals(hash)) {
                        foundPassword = password;
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
