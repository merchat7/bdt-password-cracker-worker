import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.SocketTimeoutException;

public class Message {
    public static boolean sendRequest(ObjectOutputStream dOut) {
        try {
            dOut.writeByte(2);
            dOut.writeUTF("join");
            dOut.writeUTF(String.valueOf(Worker.noCores));
            dOut.flush();
            System.out.println("Join request send to server");
            return true;
        } catch (IOException e) {
            System.out.println("Failed to send join request to server");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean receiveAckRequest(ObjectInputStream dIn) throws SocketTimeoutException {
        try {
            String messageType = dIn.readUTF();
            String message = dIn.readUTF();
            if (messageType.equals("received") && message.equals("join")) return true;
        } catch (SocketTimeoutException e) {
            // Timeout = 15 seconds, so should terminate!
            throw new SocketTimeoutException();
        } catch (IOException e) {
            System.out.println("Failed to receive ack for request from server");
            e.printStackTrace();
        }
        return false;
    }

    public static boolean receiveAckResult(ObjectInputStream dIn) throws SocketTimeoutException {
        try {
            String messageType = dIn.readUTF();
            if (messageType.equals("receivedResult")) return true;
        } catch (SocketTimeoutException e) {
            // Timeout = 15 seconds, so should terminate!
            throw new SocketTimeoutException();
        } catch (IOException e) {
            System.out.println("Failed to receive ack for result from server");
            e.printStackTrace();
        }
        return false;
    }

    public static boolean sendPing(ObjectOutputStream dOut) {
        try {
            dOut.writeByte(3);
            dOut.writeUTF("pingWorker");
            dOut.flush();
            //System.out.println("Ping send to server");
            return true;
        } catch (IOException e) {
            System.out.println("Failed to ping server");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean sendResult(ObjectOutputStream dOut, String result) {
        try {
            dOut.writeByte(4);
            dOut.writeUTF(result);
            dOut.flush();
            return true;
        } catch (IOException e) {
            System.out.println("Failed to send result to server");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean ackWork(ObjectOutputStream dOut) {
        try {
            dOut.writeUTF("receivedWork");
            dOut.flush();
            return true;
        } catch (IOException e) {
            System.out.println("Error sending ack of work to server");
            e.printStackTrace();
            return false;
        }
    }

    public static void deregister(ObjectOutputStream dOut) {
        try {
            dOut.writeByte(5);
            dOut.flush();
        } catch (IOException e) {
            System.out.println("Failed to deregister self from server");
            e.printStackTrace();
        }
    }
}
